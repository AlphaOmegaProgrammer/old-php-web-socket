This is web socket server I wrote in PHP many years ago. I want to say this used PHP 7, but I don't remember. Even if it still works, you shouldn't use this because it's blocking and single threaded and just generally not well designed.

The way this worked was you'd run `daemonize.php` from the command line and provide the expected arguments.

`master_web_socket.php` is the base websocket server implementation, and `chat.php` is a chatroom built on top of the base implementation.

`daemonize.php` would also check for changes to the files and hot reload them.
