<?php

$class = array(
	// "Properties"
	'data' => array(
		'data' => array(),
		'parent' => NULL
	),
	
	// "Public" functions
	'__construct' => function(&$parent) use (&$class){
		$class['data']['parent'] =& $parent;
	},

	'get_version' => function(){
		return '1.0.0';
	},

	'get_data' => function() use (&$class){
		return $class['data'];
	},
	
	'set_data' => function($data) use (&$class){
		$class['data'] = &$data;
	},

	'before_close' => function() use (&$class){
		$class['send_chat']($class['data']['data'][$class['data']['parent']['get_active_socket']()['name']].' has left the room!');
		$class['data']['data'][$class['data']['parent']['get_active_socket']()['name']] = NULL;
		unset($class['data']['data'][$class['data']['parent']['get_active_socket']()['name']]);
	},

	'after_close' => function() use (&$class){
		$class['send_users']();
	},

	'on_buffer' => function(&$buffer) use (&$class){
		foreach($class['data']['parent']['read_buffer']($buffer) as $message){
			$data = json_decode($message, true);
	
			if($data === NULL || !array_key_exists('action', $data))
				return;
	
			switch($data['action']){
				case 'set_username':
					$class['data']['data'][$class['data']['parent']['get_active_socket']()['name']] =& $data['username'];
					$class['send_chat']($class['data']['data'][$class['data']['parent']['get_active_socket']()['name']].' has joined the room!');
					$class['send_users']();
				break;
			
				case 'get_users':
					$class['send_users']();
						break;
	
				case 'chat':
					$class['send_chat']($class['data']['data'][$class['data']['parent']['get_active_socket']()['name']].': '.$data['message']);
				break;
	
				default:
					return;
			}
		}
	},


	// "Private" functions
	'send_chat' => function($message) use(&$class){
		$class['send']($class['data']['parent']['get_sockets'](__FILE__), json_encode(array('chat' => '['.date('H:i:s').'] '.htmlentities($message))));
	},

	'send_users' => function() use (&$class){
		$class['send']($class['data']['parent']['get_sockets'](__FILE__), json_encode(array('users' => $class['data']['data'])));
	},

	'send' => function($sockets_arr, $message) use (&$class){
		$message = $class['data']['parent']['write_buffer']($message);

		foreach($sockets_arr as $n => $s)
			socket_write($s, $message);
	}
);

return $class;