<?php


// Ensure the script was called properly
if($argc < 3 || $argc > 5)
	die("Incorrect number of arguments!\n\nUsage:\n# php script.php <bind-ipv4-or-ipv6-address> <bind-port> [document-root=".getcwd()."] [sleep-microseconds=1000]\n");

if(!filter_var($argv[1], \FILTER_VALIDATE_IP))
	die($argv[1]." is not a valid IP address!\n");

if(!array_key_exists(3, $argv))
	$argv[3] = getcwd();

if(!array_key_exists(4, $argv))
	$argv[4] = 1000;

echo "Address:\t".$argv[1]."\nPost:\t\t".$argv[2]."\nRoot:\t\t".$argv[3]."\nSleep:\t\t".$argv[4]." microseconds\n\n";


// Now for the error handling function that's going to be used when trying to reload classes



// Now for runtime. When PHP officially supports monkey patching, I'm going to rewrite this whole thing properly with OOP, but until then I'm stuck with a mess of anonymous functions and arrays
$mtime = filemtime(__DIR__.'/master_web_socket.php');

$web_socket = require('master_web_socket.php');
$web_socket['__construct']($argv[3], $argv[1], $argv[2]);

echo 'File '.preg_replace('%^'.preg_quote($argv[3]).'%','',dirname(__FILE__)).'master_web_socket.php version '.$web_socket['get_version']()." loaded\n\n";

while(true){
	usleep((int)$argv[4]);

	clearstatcache();
	if(filemtime(__DIR__.'/master_web_socket.php') != $mtime){
		$mtime = filemtime(__DIR__.'/master_web_socket.php');

		ob_start();
		system('php -l '.__DIR__.'/master_web_socket.php');
		$errors = ob_get_contents();
		ob_end_clean();

		if(substr($errors, 0, 2) == 'No'){
			$data = $web_socket['get_data']();
			$web_socket = require(__DIR__.'/master_web_socket.php');
			$web_socket['set_data']($data);

			echo 'File '.preg_replace('%^'.preg_quote($argv[3]).'%','',__DIR__).'/master_web_socket.php version '.$web_socket['get_version']()." loaded\n\n";
		}else
			echo "\nFile ".preg_replace('%^'.preg_quote($argv[3]).'%','',__DIR__)."/master_web_socket.php failed to reload due to the errors above:\n\n";
	}
	
	$web_socket['loop']();
}
