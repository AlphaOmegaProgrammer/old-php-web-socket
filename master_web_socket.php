<?php

$class = array(
	// "Properties"
	'data' => array(
		'active_socket' => NULL,
		'classes' => array(),
		'log_prefix' => NULL,
		'new_socks' => array(),
		'root' => NULL,
		'sockets' => array()
	),

	// "Public" functions
	'__construct' => function(&$root, &$address = '0.0.0.0', &$port = '8080') use (&$class){
		$class['data']['log_prefix'] = $address.':'.$port.' - ';
		$class['data']['root'] =& $root;

		echo $class['data']['log_prefix'].date('m/d/Y H:i:s'), "\n\tStarting...\n\tRAM allocated: ".number_format(memory_get_usage())." bytes\n\n";

		$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

		if(is_bool($socket))
			die("socket_create() failed!'\n");
	
		if(!socket_set_option($socket, SOL_SOCKET, SO_REUSEADDR, 1))
			die("socket_set_option() failed!\n");

		if(!socket_bind($socket, $address, $port))
			die("socket_bind() failed!\n");

		if(!socket_listen($socket))
			die("socket_listen() failed!\n");
	
		$class['data']['sockets']['master'] = array(
			'socket' => &$socket,
			'name' => 'master',
			'filename' => __FILE__
		);

		echo $class['data']['log_prefix'].date('m/d/Y H:i:s'), "\n\tSocket is listening!\n\tRAM allocated: ".number_format(memory_get_usage())." bytes\n\n";

		socket_set_nonblock($class['data']['sockets']['master']['socket']);
		$class['__construct'] = NULL;
		unset($class['__construct']);
	},

	'loop' => function() use (&$class){
		// If we have any new sockets...
		if(!empty($class['data']['new_socks'])){
			$r = $class['data']['new_socks'];
			$w = $e = array();

			socket_select($r, $w, $e, NULL);

			// we get every new socket that has something going on and perform the handshake
			foreach($r as $name => $new_sock){
				$buffer;

				socket_recv($new_sock, $buffer, 2048, MSG_WAITALL);

				if(empty($buffer))
					continue;

				$headers = array();
				foreach(explode("\n",str_replace("\r",'',$buffer)) as $line)
					if(strpos($line, ':') !== false){
						$header = explode(":",$line,2);
						$headers[strtolower(trim($header[0]))] = trim($header[1]);
					}

				// Now we find the sockets sub class. If the file doesn't exist, we abort the connection
				$file = '/'.trim($class['data']['root'],'/').'/'
						.preg_replace('%^[a-z]*://%','',$headers['origin']).'/'
						.str_replace('.','',rtrim(preg_replace('%^/wss[^/]*/%','',$headers['x-forwarded-uri']), '/'))
						.'.php';

				if(!file_exists($file))
					continue;

				// Else we read the file and instantiate the class if we haven't already done so
				clearstatcache();
				if(!array_key_exists($file, $class['data']['classes']) || $class['data']['classes'][$file]['mtime'] != filemtime($file))
					$class['set_sub_class']($file);

				// I don't understand this part at all. If you change the seemingly-random hex, the connection will always fail. The script I'm using as a reference called this string $magicGUID.
				$webSocketKeyHash = sha1($headers['sec-websocket-key'].'258EAFA5-E914-47DA-95CA-C5AB0DC85B11');
				$rawToken = "";
			
				for ($i = 0; $i < 20; $i++) 
					$rawToken .= chr(hexdec(substr($webSocketKeyHash,$i*2, 2)));

				socket_write(
					$new_sock,
					"HTTP/1.1 101 Switching Protocols\r\n".
					"Upgrade: websocket\r\n".
					"Connection: Upgrade\r\n".
					"Sec-WebSocket-Accept: ".base64_encode($rawToken)."\r\n\r\n"
				);
			
				// Finally, after finishing the handshake, we store the socket and related data then move onto the next item in the loop
				$class['data']['sockets'][] = array(
					'filename' => $file,
					'socket' => $new_sock
				);

				$key = end(array_keys($class['data']['sockets']));
				$class['data']['sockets'][$key]['name'] = $key;

				$class['data']['active_socket'] = end($class['data']['sockets']);

				$class['data']['new_socks'][$name] = NULL;
				unset($class['data']['new_socks'][$name]);
				
				if(array_key_exists('on_add', $class['data']['classes'][$file]['class']))
					$class['data']['classes'][$file]['class']['on_add']();

				continue;
			}
		}


		// Now for every existing socket that has something going on (including new sockets that were just added that iteration)
		$r = array_filter(array_map(function($arr){ return $arr['socket']; }, $class['data']['sockets']));
		$w = $e = array();

		socket_select($r, $w, $e, NULL);

		foreach(array_filter(array_map(function($arr){ return $arr['socket']; }, $class['data']['sockets'])) as $name => $socket){
			$class['data']['active_socket'] = $class['data']['sockets'][$name];

			// If it's the master socket
			if($name === 'master'){
				// Check to see if there's a new connection
				$new_conn = socket_accept($class['data']['active_socket']['socket']);

				// If not, we move on to the next item in the loop
				if(!$new_conn)
					continue;

				//  But if there is, we need to add it to $new_socks and continue to the next item in this array
				socket_set_nonblock($new_conn);
				$class['data']['new_socks'][] = $new_conn;
				continue;
			}

			// Now we reload the sub class if it has changed
			clearstatcache();
			if($class['data']['classes'][$class['data']['active_socket']['filename']]['mtime'] != filemtime($class['data']['active_socket']['filename']))
				$class['set_sub_class']($class['data']['active_socket']['filename']);

			// Now to pull the buffer. If socket_recv return 0, that means we recieved a disconnect request!
			if(in_array($socket, $r)){
				$buffer;

				if(socket_recv($class['data']['active_socket']['socket'], $buffer, 2048, MSG_WAITALL) === 0){
					$class['remove_socket']($class['data']['active_socket']['socket']);
					continue;
				}
			}

			// Now if the buffer is simply empty, we move on
			if(isset($buffer) && !empty($buffer) && array_key_exists('on_buffer', $class['data']['classes'][$class['data']['active_socket']['filename']]['class']))
				$class['data']['classes'][$class['data']['active_socket']['filename']]['class']['on_buffer']($buffer);
			
			if(array_key_exists('on_loop', $class['data']['classes'][$class['data']['active_socket']['filename']]['class']))
				$class['data']['classes'][$class['data']['active_socket']['filename']]['class']['on_loop']();

		}

		echo $class['data']['log_prefix'].date('m/d/Y H:i:s')."\n\tRAM allocated: ".number_format(memory_get_usage())." bytes\n\tSocket count: ".(count($class['data']['sockets'])-1)."\n\t".'Class count: '.(count($class['data']['classes']))."\n\n";
	},

	'get_active_socket' => function() use (&$class){
		return $class['data']['active_socket'];
	},

	'get_data' => function() use (&$class){
		return $class['data'];
	},

	'get_sockets' => function($filename) use (&$class){
		return array_filter(array_map(function($arr) use (&$filename, &$class){if($filename === $arr['filename']) return $arr['socket']; }, $class['data']['sockets']));
	},

	'get_version' => function(){
		return '1.0.0';
	},

	'remove_socket' => function(&$socket = null) use (&$class){
		if(!isset($socket))
			$socket =& $class['data']['active_socket']['socket'];

		$key = NULL;
		foreach($class['data']['sockets'] as $name => $arr)
			if($socket === $arr['socket']){
				$key = $name;
				break;
			}

		if(!isset($key) || $key === 'master')
			return;


		$filename = $class['data']['sockets'][$key]['filename'];
		$subclass = $class['data']['classes'][$filename]['class'];
		if(array_key_exists('before_close', $subclass))
			$subclass['before_close']();

		socket_close($socket);
		$class['data']['sockets'][$key] = NULL;
		unset($class['data']['sockets'][$key]);

		if(array_key_exists('after_close', $subclass))
			$subclass['after_close']();
	},

	'set_data' => function(&$data) use (&$class){
		$class['data'] =& $data;
	},

	'set_sub_class' => function(&$filename) use (&$class){
		if(!array_key_exists($filename, $class['data']['classes']))
			$class['data']['classes'][$filename] = array();

		$class['data']['classes'][$filename]['mtime'] = filemtime($filename);

		ob_start();
		system('php -l '.escapeshellarg($filename));
		$errors = ob_get_contents();
		ob_end_clean();

		if(substr($errors, 0, 2) == 'No'){
			if(array_key_exists('class', $class['data']['classes'][$filename]))
				$data = $class['data']['classes'][$filename]['class']['get_data']();
			
			$func = function(&$filename){
				return require($filename);
			};

			$class['data']['classes'][$filename]['class'] = $func($filename);
			$class['data']['classes'][$filename]['class']['__construct']($class);

			if(isset($data))
				$class['data']['classes'][$filename]['class']['set_data']($data);

			echo 'File '.$filename.' version '.$class['data']['classes'][$filename]['class']['get_version']()." loaded\n\n";
		}else
			echo "\nFile ".$filename." failed to reload due to the errors above:\n\n";
	},


	'write_buffer' => function(&$message){
		$length = strlen($message);
		$length_field = "";
	
		if($length >= 126){
			foreach(str_split(strrev('0'.dechex($length)), 2) as $hex){
				if(strlen($hex) == 2)
					$length_field = chr(hexdec(strrev($hex))).$length_field;
			}

		    if($length <= 65536){
		    	$length = 126;

		    	while(strlen($length_field) < 2)
		        	$length_field = chr(0).$length_field;
			}else{
				$length = 127;
				
				while(strlen($length_field) < 8)
					$length_field = chr(0).$length_field;
			}
		}
	
		return chr(129).chr($length).$length_field.$message;
	},

	'read_buffer' => function(&$buffer) use (&$class){
		// First of all, if the buffer is full of ASCII, it's a header and we don't need to parse it
		if(mb_detect_encoding($buffer) == 'ASCII')
			return array();

		// This is an array becasue it's possible to recieve multiple frames per iteration of the daemon loop
		$payloads = array();

		while($buffer !== false){
			// This is the length of all of the headers
			$offset = 2;

			// If there's a mask...
			if($buffer[1] & chr(128)){
				$mask = $buffer[2].$buffer[3].$buffer[4].$buffer[5];
				$offset += 4;
			}

			// Get the payload's length
			$length = ord($buffer[1]) % 128;

			// If $length is 0, it's a request from the client to disconnect the socket
			if(!$length){
				$this->remove_socket();
				return false;
			}

			// If $length is exactly 126 or 127, that means the header is a little longer than we first thought
			if($length == 126){
				if(isset($mask))
					$mask = $buffer[4].$buffer[5].$buffer[6].$buffer[7];

				$offset += 2;
				$length = ord($buffer[2])*256+ord($buffer[3]);
			}elseif($length == 127){
				if(isset($mask))
					$mask = $buffer[10].$buffer[11].$buffer[12].$buffer[13];

				$offset += 8;
				$length = ord($buffer[2]) * 72057594037927936
					+ ord($buffer[3]) * 281474976710656
					+ ord($buffer[4]) * 1099511627776
					+ ord($buffer[5]) * 4294967296
					+ ord($buffer[6]) * 16777216
					+ ord($buffer[7]) * 65536
					+ ord($buffer[8]) * 256
					+ ord($buffer[9]);
			}

			// If our buffer isn't complete, we have to keep pulling from the buffer until we get the whole thing
			while(strlen($buffer) < $offset + $length){
				$tmp_buffer;
				socket_recv($class['data']['active_socket']['socket'], $tmp_buffer, 2048, MSG_WAITALL);
				$buffer .= $tmp_buffer;
			}

			// Now that we've figured out how long this frame is, we need to skip it if the reserved bits are set
			if(ord($buffer[0] & chr(64)) + ord($buffer[0] & chr(32)) + ord($buffer[0] & chr(16))){
				$buffer = substr($buffer,$offset+$length);
				continue;
			}

			// Pull the payload out of the frame
			$payload = substr($buffer,$offset, $length);
		
			// Now if there's a mask, we need to Xor the mask with the payload to get the actual text string.
			if(isset($mask))
				// To Xor the payload with 4-byte mask, we need the mask to be the same length as the payload by repeating the mask until it's at least equal to the length of the payload then chop off the excess binary
				$payload = substr(str_repeat($mask, ceil(strlen($payload)/strlen($mask))), 0, strlen($payload)) ^ $payload;
	  
	  		// If the message is 2 bytes of UTF-8 garbage, that seems to mean that the client closed their browser/tab without properly closing the socket connection.
			if(mb_detect_encoding($payload) === 'UTF-8' && strlen($payload) === 2){
				$class['remove_socket']();
				return array();
			}	  
	  
			$payloads[] = $payload;

			// Now we remove this frame from the buffer and continue looping until $buffer is empty
			$buffer = substr($buffer,$offset+$length);
		}

		return $payloads;
	}
);

return $class;
